#coding: utf-8

# 第一層標題
m1 = "<a href='index'>有關本站</a>"
m11 = "<a href='Bry'>Brython運算</a>"
# 第一層標題
m2 = "<a href=''>組員介紹</a>"
m21 = "<a href='/introMember1'>49923255</a>"
m22 = "<a href='/introMember2'>49923254</a>"
m23 = "<a href='/introMember3'>49923243</a>"
m24 = "<a href='/introMember4'>49923240</a>"
m25 = "<a href='/introMember5'>49923219</a>"
m26 = "<a href='/introMember6'>40023234</a>"
# 第一層標題
m3 = "<a href=''>w13-test</a>"
m31 = "<a href=''>Creo</a>"
m311 = "<a href='creo'>零件繪圖</a>"
m312 = "<a href='creoprt'>零件組立</a>"
m313 = "<a href=''>Pro/Web.Link</a>"
m32 = "<a href='solidedge'>Solid Edge</a>"
m33 = "<a href='Solvespace'>Solvespace</a>"
m34 = "<a href='Solvespaceprt'>Solvespace組立</a>"
m35 = "<a href='Brython'>Brython程式</a>"
m36 = "<a href='VREP'>VREP</a>"
# 第一層標題
m4 = "<a href=''>w18-test</a>"
m41 = "<a href='http://c1project-49923255.rhcloud.com/cmslite/'>c1project</a>"
m42 = "<a href='final'>期末</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 =[[m1, m11],[m2, m21, m22, m23, m24,m25,m26],[m3, [m31, m311, m312, m313], m32, m33, m34,m35,m36],[m4, m41,m42]]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
